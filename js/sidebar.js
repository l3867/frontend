class load extends HTMLElement {
  connectedCallback() {
	  fetch(
		  "components/sidebar.html",
		  {
			  method: "GET",
			  mode: "no-cors"
		  })
	  .then(response => response.text())
	  .then(content => this.innerHTML = content)
  }
}

customElements.define('load-comp', load);






function sb_collapse()
{
	document.getElementById("sb-burger").classList.toggle("change");
	var sb_col = document.getElementById("sb"); 
	sb_col.classList.toggle("sb-collapsed");
	sb_col.classList.toggle("sb");
	
	var burger = document.getElementsByClassName("sb-burger")[0];
	burger.style.right = burger.style.right === "40%" ? "0" : "40%";

	var profile = document.getElementById("sb-user-logo");
	profile.style.display = profile.style.display === "none" ? "flex" : "none";

	var btn = document.getElementsByClassName("btn-text");
	for (var i = 0; i < btn.length; i++)
	{
		
		var tmp = btn[i];
		tmp.classList.toggle("tooltip");
	}

	var icon = document.getElementsByClassName("icon");
	for (var i = 0; i < icon.length; i++)
	{
		var tmp = icon[i];
		tmp.style.width = tmp.style.width === "90%" ? "20%" : "90%";
	}

	var logo = document.getElementById("sb-logo-png");
	logo.classList.toggle("sb-logo-change");


}

