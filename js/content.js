class content extends HTMLElement {
  connectedCallback() {
	  fetch(
		  "components/content.html",
		  {
			  method: "GET",
			  mode: "no-cors"
		  })
	  .then(response => response.text())
	  .then(content => this.innerHTML = content)
  }
}

customElements.define('load-content', content);
